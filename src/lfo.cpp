#include "lfo.h"
#include "protocol.h"

extern Patch::Protocol *protocol;

Patch::Lfo::Lfo() 
{
  // dummy values
  waveform.shape = Square;
  //waveform.bpm = 60.0;
  //waveform.period = 0.125;
  waveform.wavelengthMsec = 100;
  waveform.pulseWidth = 0.6;
  waveform.offset = 15;
  waveform.delayMsec = 0;
  waveform.amplitude = 50;
  waveform.numBeats = 0;
  
  midi.channel = 1;
  midi.cc = 7;
  midi.resolution = 1000; // in Hz
  midi.keyFollow = 0;
}

void Patch::Lfo::startLfo() 
{
  waveStartMillis = nextValueMillis = millis();
  //waveLengthMillis = (waveform.bpm / 60.0) * waveform.period * 1000.0;
  waveValueCount = 0;
  running = 1;
}

void Patch::Lfo::haltLfo() 
{
  running = 0;
}

void Patch::Lfo::reset(uint8_t channel) {
  if (midi.channel == channel && midi.keyFollow) {
    waveStartMillis = millis();
    nextValueMillis = millis() - 1;
    waveValueCount = 0;  
  }
}

void Patch::Lfo::doLfo() 
{
  if (!running) {
    return;
  }

  if (waveStartMillis + waveform.wavelengthMsec < millis()) {
    Serial.print("Wave reset, WaveStartMillis: ");
    Serial.print(waveStartMillis, HEX);
    Serial.print(", WaveLengthMsec: ");
    Serial.println(waveform.wavelengthMsec, HEX);

    // reset the wave to start point    
    waveStartMillis = millis();
    nextValueMillis = millis() - 1;
    waveValueCount = 0;
  }
  if (nextValueMillis < millis()) {
    uint8_t value = calculateNextValue();
    usbMIDI.sendControlChange(midi.cc, value, midi.channel);

    Serial.print("CC: ");
    Serial.println(value, HEX);

    // protocol->logLocalMessageToPi("CCxx", midi.channel, midi.cc, value);

    nextValueMillis += (long)(1000.0 / midi.resolution);
  }
}

/*
 * Return waveform points to Pi for display 
 */
void Patch::Lfo::lfoGraphPoints() 
{
    printProgram();

    // long waveLengthMillis = ((waveform.bpm * 1000) / 60.0) * waveform.period;
    // Serial.print("waveLengthMillis: ");
    // Serial.println(waveLengthMillis);
 
    // Simulate the real loop to create the graph points. Max hz is 1000. 
    float nextValueMillis = 0;
    float msecPerValue = max(1.0, waveform.wavelengthMsec / 100.0);
    long count = 0;

    Serial.print("LFOG, msecPerValue: ");
    Serial.println(msecPerValue);
 
    // msecPerValue must be at least 1
    // must be no more than 100 values

    for (long millis=0; millis<waveform.wavelengthMsec; millis++) {
        if (nextValueMillis <= millis) {
            toHex(&buffer[count], nextValueMillis, 4);
            count = count + 4;
            uint8_t val = calculateNextSquareValue(nextValueMillis);
            toHex(&buffer[count], calculateNextSquareValue(nextValueMillis), 2);
            count = count + 2;

            buffer[count] = 0;

            Serial.print("NextValMillis: ");
            Serial.print((uint32_t) nextValueMillis);
            Serial.print(", value: ");
            Serial.print(val);
            Serial.print(", Hex: ");
            Serial.println((char *)&buffer[count-6]);
            nextValueMillis = millis + msecPerValue;
        }
    }
    Serial.println("LFOG Writing buffer ");

    long length = count + 4;

    PI_SERIAL.write(0x12);
    PI_SERIAL.print("T");
    PI_SERIAL.write(length >> 8 & 0xff);
    PI_SERIAL.write(length & 0xff);
    PI_SERIAL.print("LFOG");
    PI_SERIAL.write(buffer, count);
    
    PI_SERIAL.write(0x0);
    PI_SERIAL.write(0x13);
    Serial.print("LFOG Done count ");
    Serial.println(count/6, DEC);
}

void Patch::Lfo::lfoProgram(LfoProgram * program) 
{
  midi.channel = program->channel;
  midi.cc = program->cc;
  midi.resolution = program->resolution;
  midi.keyFollow = program->keyFollow;
  waveform.wavelengthMsec = program->wavelengthMsec;
  waveform.amplitude = program->amplitude;
  waveform.offset = program->offset;
  waveform.delayMsec = program->delayMsec;
  waveform.pulseWidth = program->pulseWidth / 100.0;

  printProgram();
}

void Patch::Lfo::printProgram() {
  Serial.print("Protocol:lfoProgram, channel: ");
  Serial.print(midi.channel);
  Serial.print(", cc: ");
  Serial.print(midi.cc);
  Serial.print(", keyFollow: ");
  Serial.println(midi.keyFollow);
  Serial.print("resolution: ");
  Serial.print(midi.resolution);
  Serial.print(", waveLengthMsec: ");
  Serial.print(waveform.wavelengthMsec);
  Serial.print(", amplitude: ");
  Serial.print(waveform.amplitude);
  Serial.print(", offset: ");
  Serial.print(waveform.offset);
  Serial.print(", delayMsec: ");
  Serial.print(waveform.delayMsec);
  Serial.print(", pulseWidth: ");
  Serial.println(waveform.pulseWidth);
}

uint8_t Patch::Lfo::calculateNextValue() 
{
  if (waveform.shape == Sine) {
    return calculateNextSineValue();
  } 
  else if (waveform.shape == Square) {
    long x = millis() - waveStartMillis;
    return calculateNextSquareValue(x);
  }
  return 0;
}

uint8_t Patch::Lfo::calculateNextSineValue() { return 0 ; }

uint8_t Patch::Lfo::calculateNextSquareValue(long xMillis) 
{
    // float wavelengthMillis = (waveform.bpm / 60) * waveform.period * 1000;
    float positiveWidthMillis = waveform.wavelengthMsec * waveform.pulseWidth;
    Serial.print("calc, positiveWidthM: ");
    Serial.print(positiveWidthMillis);
    Serial.print(", xMillis: ");
    Serial.print(xMillis);

    float y = min( MAX_Y,
        (( between( xMillis, waveform.delayMsec, positiveWidthMillis + waveform.delayMsec )) // 0 or 1
        * waveform.amplitude ) // 0 or amplitude
        + waveform.offset); // add the starting yOffset

    Serial.print(", y: ");
    Serial.println(y);

    return (char) y; // convert back to 0-127
}

uint8_t Patch::Lfo::between(long value, long lower, long upper) 
{
    return (value > lower) && (value <= upper);
}

uint8_t Patch::Lfo::sign(uint8_t x) 
{
    return x <= 0 ? 0 : 1; 
}

void Patch::Lfo::toHex(uint8_t *buf, float x, uint8_t numDigits) {
    static const char* digits = "0123456789ABCDEF";
    uint32_t val = (uint32_t) x;
    uint8_t shiftVal = (numDigits - 1) * 4;
    for (int i=0; i<numDigits; i++) {
      buf[i] = digits[(val >> shiftVal) & 0xf];
      shiftVal -= 4;
    }
}