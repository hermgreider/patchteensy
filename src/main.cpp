#include <Arduino.h>
#include "midireceive.h"
#include "protocol.h"
#include "lfo.h"
#include "flicker.h"

Patch::MidiReceive *midiReceive;
Patch::Protocol *protocol;
Patch::Lfo *lfo;
Patch::Flicker *flicker;
long start = millis();

void setup()
{
  Serial.begin(9600);

  protocol = new Patch::Protocol();
  midiReceive = new Patch::MidiReceive();
  lfo = new Patch::Lfo();

  flicker = new Patch::Flicker();
  flicker->flickerSlow(4);
}

void loop()
{
  midiReceive->checkMidi();
  protocol->checkForPiMessage();
  lfo->doLfo();
  flicker->doFlicker();

  if ((millis() - start > 1000) && (start != 0)) {
    start = 0;
    Serial.println("Patch Alive");
  }
}

void toHexString(uint8_t *buf, uint8_t in)
{
  uint8_t hex_str[]= "0123456789ABCDEF";
  buf[0] = hex_str[(in >> 4) & 0x0F];
  buf[1] = hex_str[in & 0x0F];
}
