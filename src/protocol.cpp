#include "protocol.h"
#include "flicker.h"
#include "lfo.h"

extern Patch::Flicker *flicker;
extern Patch::Lfo *lfo;

Patch::Protocol::Protocol()
{
  // Configure PI_SERIAL speed
  PI_SERIAL.begin(115200);
}

void Patch::Protocol::checkForPiMessage()
{
  // Commands from Pi
  if (PI_SERIAL.available() > 0)
  {
    handlePiCommand();
  }
}

void Patch::Protocol::handlePiCommand()
{
  uint8_t data = PI_SERIAL.read();

  flicker->flickerFast(3);

  if (data == 0x12)
  {
    Serial.println("START");
    commandCount = 0;
  }
  else if (data == 0x13)
  {
    uint8_t *command = (uint8_t *)&piCommand[3];

    Serial.print("END, command: ");
    Serial.println((char *)command);

    if (strncmp((char *)command, "CONN", 4) == 0) 
    {
      Serial.println("CONN");
      logAckToPi();
    }
    else if (strncmp((char *)command, "NOON", 4) == 0) 
    {
      note(1, command);
      logAckToPi();
    }
    else if (strncmp((char *)command, "NOFF", 4) == 0) 
    {
      note(0, command);
      logAckToPi();
    }
    else if (strncmp((char *)command, "CCxx", 4) == 0) 
    {
      cc(command);
      logAckToPi();
    }
    else if (strncmp((char *)command, "PCxx", 4) == 0) 
    {
      pc(command);
      logAckToPi();
    }
    else if (strncmp((char *)command, "LFOS", 4) == 0) 
    {
      lfoStart();
      logAckToPi();
    }
    else if (strncmp((char *)command, "LFOG", 4) == 0) 
    {
      lfoGraphPoints();
    }
    else if (strncmp((char *)command, "LFOH", 4) == 0) 
    {
      lfoHalt();
      logAckToPi();
    }
    else if (strncmp((char *)command, "LFOP", 4) == 0) 
    {
      lfoSaveProgram(command);
      logAckToPi();
    }

    commandCount = 0;
  }
  else if (commandCount < 512) 
  {
    piCommand[commandCount++] = data;
    Serial.print("UART received: 0x");
    Serial.println(data, HEX);
  }
  else 
  {
    Serial.print("UART received TOO MANY BYTES: 0x");
    Serial.println(data, HEX);
  }
}

void Patch::Protocol::logAckToPi()
{
  PI_SERIAL.write(0x12);
  PI_SERIAL.write("T"); // Source = Teensy
  PI_SERIAL.write(0);   // Msg length 1
  PI_SERIAL.write(0x4); // Msg length 0
  PI_SERIAL.print("ACKx");
  PI_SERIAL.write(0);
  PI_SERIAL.write(0x13);
}

void Patch::Protocol::logMessageToPi(uint8_t *message) 
{
  //Serial.print("To Pi: ");

  for(int i = 0; message[i] != 0x13; i++) 
  {
    PI_SERIAL.write(message[i]);
    //Serial.print(message[i], HEX);
    //Serial.print(",");
  }
  PI_SERIAL.write(0x13);
  //Serial.println(0x13, HEX);
}

// NOON/NOFF - channel, note, velocity
void Patch::Protocol::note(uint8_t on, uint8_t *command) 
{
  uint32_t noteVelocity = strtol((char *)&command[4], 0, 16);

  uint8_t velocity = noteVelocity & 0xff;
  uint8_t note = (noteVelocity & 0xff00) >> 8;
  uint8_t channel = (noteVelocity & 0xff0000) >> 16;

  if (on) 
  {
    usbMIDI.sendNoteOn(note, velocity, channel);
    Serial.print("Note On: ");
  }
  else 
  {
    usbMIDI.sendNoteOff(note, 0, channel);
    Serial.print("Note Off: ");
  }

  Serial.print("note: ");
  Serial.print(note, HEX);
  Serial.print(", velocity: ");
  Serial.print(velocity, HEX);
  Serial.print(", channel: ");
  Serial.println(channel, HEX);
}

// CCxx - channel, CC#, value
void Patch::Protocol::cc(uint8_t *command) 
{
  uint32_t ccCmd = strtol((char *)&command[4], 0, 16);

  uint8_t value = ccCmd & 0xff;
  uint8_t control = (ccCmd & 0xff00) >> 8;
  uint8_t channel = (ccCmd & 0xff0000) >> 16;

  usbMIDI.sendControlChange(control, value, channel);
  Serial.print("CC: ");

  Serial.print("control: ");
  Serial.print(control, HEX);
  Serial.print(", value: ");
  Serial.print(value, HEX);
  Serial.print(", channel: ");
  Serial.println(channel, HEX);
}

// PCxx - channel, PC#
void Patch::Protocol::pc(uint8_t *command) 
{
  uint32_t ccCmd = strtol((char *)&command[4], 0, 16);

  uint8_t program = ccCmd & 0xff;
  uint8_t channel = (ccCmd & 0xff00) >> 8;

  usbMIDI.sendProgramChange(program, channel);
  Serial.print("PC: ");

  Serial.print("program: ");
  Serial.print(program, HEX);
  Serial.print(", channel: ");
  Serial.println(channel, HEX);
}

// LFOS - channel, PC#
void Patch::Protocol::lfoStart() 
{
  Serial.print("LFOS: ");
  lfo->startLfo(); 
}

void Patch::Protocol::lfoGraphPoints() 
{
  Serial.print("LFOG: ");
  lfo->lfoGraphPoints();
}

void Patch::Protocol::lfoHalt() 
{
  Serial.print("LFOH: ");
  lfo->haltLfo();
}

uint32_t Patch::Protocol::convert(uint8_t *array, uint8_t numBytes) 
{
  uint32_t value = 0;
  for (uint32_t i=0; i<numBytes; i++) {
      value *= 0x10;

      uint16_t current = (uint8_t)array[i];
      if (array[i] >= 0x61) {
        value += current - 0x61 + 0xa;
      }
      else if (array[i] >= 0x41) {
        value += current - 0x41 + 0xa;
      }
      else {
        value += current - 0x30;
      }
  }
  return value;
}

// LFOP[channel][wavelengthMsec1][wavelengthMsec0][amplitude]
void Patch::Protocol::lfoSaveProgram(uint8_t *command) 
{
  LfoProgram *lfoProgram = new LfoProgram();
  lfoProgram->channel = convert(&command[4], 2);
  lfoProgram->cc = convert(&command[6], 2);
  lfoProgram->wavelengthMsec = convert(&command[8], 4);
  lfoProgram->resolution = convert(&command[12], 4);
  lfoProgram->amplitude = convert(&command[16], 2);
  lfoProgram->offset = convert(&command[18], 2);
  lfoProgram->delayMsec = convert(&command[20], 2);
  lfoProgram->pulseWidth = convert(&command[22], 2);
  lfoProgram->keyFollow = convert(&command[24], 2);

  lfo->lfoProgram(lfoProgram);
}

void Patch::Protocol::logLocalMessageToPi(const char *type, uint8_t channel, uint8_t data1, uint8_t data2) 
{
  uint8_t message[20] = { 0x12, 'T', 0, 0xa };

  memcpy(&message[4], type, 4);
    
  toHexString(&message[8], channel);
  toHexString(&message[10], data1);
  toHexString(&message[12], data2);
  message[14] = 0;
  message[15] = 0x13;

  logMessageToPi(message); 
}