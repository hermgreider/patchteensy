#include "midireceive.h"
#include "protocol.h"
#include "lfo.h"

extern Patch::Protocol *protocol;
extern Patch::Lfo *lfo;

Patch::MidiReceive::MidiReceive() { }

// Called from main loop to check for receive MIDI messages 
void Patch::MidiReceive::checkMidi() {
  if (usbMIDI.read()) {
    uint8_t message[20] = { 0x12, 'T', 0, 0xa };

    switch(usbMIDI.getType()) {
      case 0x80: 
        memcpy(&message[4], "NOFF", 4);
        break;
      case 0x90: 
        memcpy(&message[4], "NOON", 4);
        break;
      case 0xB0:
        memcpy(&message[4], "CCxx", 4);
        break;
      case 0xC0:
        memcpy(&message[4], "PCxx", 4);
        break;
      default:
        memcpy(&message[4], "UNKN", 4);
        break;
    }
    
    uint8_t channel = usbMIDI.getChannel();
    toHexString(&message[8], channel);
    toHexString(&message[10], usbMIDI.getData1());
    toHexString(&message[12], usbMIDI.getData2());
    message[14] = 0;
    message[15] = 0x13;

    protocol->logMessageToPi(message); 

    lfo->reset(channel);
  }
}

