#include "flicker.h"

Patch::Flicker::Flicker() {
  // initialize LED digital pin as an output
  pinMode(LED_BUILTIN, OUTPUT);
}

// Called from main loop to check if it's time for a flicker change
void Patch::Flicker::doFlicker() {
  if ((times > 0) && (millis() - lastUpdateMsec > updateMsec)) {
    --times;
    lastUpdateMsec = millis();
    ledState = ledState == HIGH ? LOW : HIGH;
    digitalWrite(LED_BUILTIN, ledState);
  }
}

void Patch::Flicker::flickerFast(uint8_t times) {
  startFlicker(times, 120);
}

void Patch::Flicker::flickerSlow(uint8_t times) {
  startFlicker(times, 500);
}

// Starts the flicker
void Patch::Flicker::startFlicker(uint8_t flickerTimes, uint32_t msec) {
  lastUpdateMsec = millis();
  updateMsec = msec; 
  ledState = LOW;
  times = flickerTimes * 2;

  digitalWrite(LED_BUILTIN, LOW);
}
