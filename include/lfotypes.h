#ifndef LFOTYPES_H
#define LFOTYPES_H

#include <Arduino.h>

namespace Patch {
  
typedef enum { Sine, Triangle, Square, RampUp, RampDown, Heartbeat } LfoShape; 

struct LfoProgram {
  uint8_t channel;   // MICI channel. 0-15.
  uint8_t cc;        // MIDI CC. 0-127.
  uint16_t wavelengthMsec;
  uint16_t resolution; // number of CC messages sent in single wave. 10-1000.
  uint8_t amplitude; // height of wave. 0-127.
  uint8_t offset;    // vertical offset of wave from 0. 0-127.
  uint8_t delayMsec; // delay from start of wave. 0-ff. Effectively phase.
  uint8_t pulseWidth;// if wave shape = square, percent of wave that is high. 0-100.
  uint8_t keyFollow; // if 1, wave restarts with each note on received on channel 
};

struct LfoWaveForm {

  LfoShape shape;

  /* 
    * Wavelength Msec
    */
  uint16_t wavelengthMsec;

  /* 
    * Number of beats per minute
    * Normally integer, but may have fractional value 
    */
  float bpm;

  /* 
    * Period: In BPM (1/16, 1/8, 1/8T, 1/4, 1/2, 1, 2, 4) = (0.0675, 0.125, 0.25/3, 0.25, 0.5)
    * Period Seconds will be (bpm / 60) * period = time for one full wave period
    */
  float period;

  /* 
    * if square, pulsewidth indicates duty cycle - percent of time the wave is high
    * 0.5 is equal on and off time
    * float between 0-1
    */
  float pulseWidth;

  // offsetY for the entire wave - minimum value for the wave
  // integer between 0-127 (because MIDI values are integers between 0-127)
  uint8_t offset;

  // offsetX - number of millis to offset the wave - this is phase
  uint8_t delayMsec;

  // height or depth of wave
  // if offsetY + amplitude > 127, top of wave is flattened to 127
  // 0-127
  uint8_t amplitude;

  // Number of beats (1-127, 0 = continuous) to run the LFO
  // Total length of waveform will be BPM * numBeats
  char numBeats;
};

struct LfoMidi {

  // Send waveform CC on channel
  // 0-15
  uint8_t channel;

  // Send waveform value on CC
  // 0-127
  char cc;

  /* 
    * Resolution in Hz
    *   Number of points sent per second
    *   Higher is smoother
    *   MIDI max is 1000 CC messages/second
    */
  float resolution;

  char keyFollow;

  /*
    * Still need:
      ○ Clock: Master or Slave or None
      ○ Sample ms
      ○ Start Channel 
      ○ Start Note (0-127)
      ○ Key Follow on/off (or call it retrigger?)
      ○ Key Follow Channel In 
      ○ Key Follow Delay - after trigger to start LFO (in msec). Stop current LFO during this time.
  */
};
}

#endif