#ifndef FLICKER_H
#define FLICKER_H

#include <Arduino.h>


namespace Patch {

class Flicker {

private:
  uint8_t  ledState = LOW;   // Whether LED pin is high or low
  uint32_t lastUpdateMsec = 0;   // start of current state
  uint32_t updateMsec = 100; // interval at which to blink (milliseconds)
  uint8_t times = 0;         // each count is a toggle - ex: 4 is on/off/on/off

public:
  Flicker();

  void doFlicker();
  void flickerFast(uint8_t times);
  void flickerSlow(uint8_t times);
  void startFlicker(uint8_t times, uint32_t msec);
};
} // namespace Patch

#endif