#ifndef MIDI_RECEIVE_H
#define MIDI_RECEIVE_H

#include <Arduino.h>

// Handles incoming MIDI messages

void toHexString(uint8_t *buf, uint8_t in);

namespace Patch {
class MidiReceive {

private:
 
public:
  MidiReceive();
  void checkMidi();
};
}
 
#endif