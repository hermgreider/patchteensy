#ifndef LFO_H
#define LFO_H

#include <Arduino.h>
#include "lfotypes.h"

#define MAX_Y 127

namespace Patch {

class Lfo {

private:
  uint8_t running = 0;
  
  LfoWaveForm waveform;
  LfoMidi midi;
  uint8_t buffer[1000];

  uint32_t waveStartMillis = 0;
  uint32_t nextValueMillis = 0;
  //uint32_t waveLengthMillis = 0;
  uint32_t waveValueCount = 0;

  void printProgram();
  uint8_t calculateNextValue();
  uint8_t calculateNextSineValue();
  uint8_t calculateNextSquareValue(long xMillis);
  uint8_t sign(uint8_t x);
  uint8_t between(long value, long lower, long upper);
  void toHex(uint8_t *buf, float x, uint8_t numDigits);

  /*
    * ADSR: On/Off, trigger must be true
    * A - depth (0-127), D - time in msec, S - depth (0-127), R - time (0-infinite)
    * Mixed with main LFO waveform
    */
//    LfoAdsr adsr;

public:
  Lfo();

  void startLfo();
  void haltLfo();
  void doLfo();
  void reset(uint8_t channel);

  void lfoGraphPoints();
  void lfoProgram(LfoProgram * program);
};
} // namespace Patch

#endif
