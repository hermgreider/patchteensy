#ifndef PROTOCOL_H
#define PROTOCOL_H

#include <Arduino.h>
#include "lfotypes.h"

// Implements serial protocol between Teensy and Pi

void toHexString(uint8_t *buf, uint8_t in);

#define PI_SERIAL Serial2

namespace Patch {
class Protocol {

private:
  uint8_t piCommand[512];   // stores the incoming bytes from Pi
  uint8_t commandCount = 0; // Index into piCommand of current byte

  void handlePiCommand();
  void logAckToPi();
  void note(uint8_t on, uint8_t *command);
  void cc(uint8_t *command);
  void pc(uint8_t *command);
  void lfoStart();
  void lfoHalt();
  void lfoGraphPoints(); 
  void lfoSaveProgram(uint8_t *command);
  uint32_t convert(uint8_t *array, uint8_t numBytes);
 
public:
  Protocol();
  void checkForPiMessage();
  void logMessageToPi(uint8_t* message);
  void logLocalMessageToPi(const char *type, uint8_t channel, uint8_t data1, uint8_t data2);
};
}
 
#endif